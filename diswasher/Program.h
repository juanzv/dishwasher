//
//  program.h
//  diswasher
//
//  Created by Juan Zenón on 6/10/19.
//  Copyright © 2019 juan. All rights reserved.
//

#ifndef program_h
#define program_h


class Program
{
    
public:
    std::string name;
    int length;
    int temperature;
    int energy;
    
    Program(std::string a, int b, int c, int d, int e) {
        name = a;
        length = b;
        temperature = c;
        energy = e;
    };
    ~Program();
    
};

#endif /* program_h */
