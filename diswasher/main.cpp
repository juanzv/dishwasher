//
//  main.cpp
//  diswasher
//
//  Created by Juan Zenón on 6/10/19.
//  Copyright © 2019 juan. All rights reserved.
//

#include <list>
#include <iostream>
#include <string>
#include <unistd.h>
#include <sstream>
#include <stdio.h>
#include <fstream>


#include "ConsoleView.h"
#include "Dishwasher.h"
#include "Program.h"
#include "Controler.h"


int main(int argc, char const *argv[])
{
    ConsoleView* console_view;
    Dishwasher* dishwasher;
    console_view = new ConsoleView();
    dishwasher = new Dishwasher();
    dishwasher->run(console_view);
    
    return 0;
}
